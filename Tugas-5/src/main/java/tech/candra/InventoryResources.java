package tech.candra;

import java.util.List;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import tech.candra.models.Inventory;
import tech.candra.repo.InventoryRepo;

@Path("/inventory")
public class InventoryResources {

    @Inject
    InventoryRepo inventoryRepo;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Inventory> getInventories() {
        return inventoryRepo.getInventories();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Inventory getInventory(@PathParam("id") int id) {
        return inventoryRepo.getInventory(id);
    }

    @POST
    @Path("/add")
    @Consumes(MediaType.APPLICATION_JSON)
    public void addInventory(@Valid Inventory inventory) {
        inventoryRepo.addInventory(inventory);
    }

    @PUT
    @Path("/{id}/set")
    @Consumes(MediaType.APPLICATION_JSON)
    public void updateInventory(@Valid @PathParam("id") int id, Inventory inventory) {
        inventoryRepo.updateInventory(id, inventory);
    }

    @DELETE
    @Path("/{id}/delete")
    @Consumes(MediaType.TEXT_PLAIN)
    public void deleteInventory(@PathParam("id") int id) {
        inventoryRepo.deleteInventory(id);
    }

    @DELETE
    @Path("/delete")
    public void deleteAll() {
        inventoryRepo.deleteInventories();
    }
}