package tech.candra;

import tech.candra.models.Mugiwara;

import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.GET;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;


@Path("mugiwara")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)

public class MugiwaraResource {

    public Map<Integer, Mugiwara> kumpulanMugiwara = new HashMap<>();
    public Integer id = 1;

    // Constructor
    //Menampung Kumpulan Object
    public MugiwaraResource() {
        Mugiwara nakama1 = new Mugiwara("Monkey D Luffi", "Akuma No Mi", "Housoku No Haki - Kenbunsouku Haki - Bousoku Haki", 100);
        Mugiwara nakama2 = new Mugiwara("Roronoa Zoro", "Santoryu", "Housoku No Haki - Bousoku Haki", 95);
        Mugiwara nakama3 = new Mugiwara("Vinsmoke Sanji", "Double Jambe", "Kenbunsouku Haki - Bousoku Haki", 90);

        kumpulanMugiwara.put(id++, nakama1);
        kumpulanMugiwara.put(id++, nakama2);
        kumpulanMugiwara.put(id++, nakama3);
    }

    @GET
    //Menampilkan daftar objek secara keseluruhan
    public Map<Integer, Mugiwara> getKumpulanMugiwara() {
        return kumpulanMugiwara;
    }

    @GET
    @Path("{id}")
    // Menampilkan Objek dengan id tertentu
    public Mugiwara getMugiwaraById(@PathParam("id") Integer id) {
        return kumpulanMugiwara.get(id);
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    //Menambahkan data objek baru
    public String addMugiwara (Mugiwara mugiwara) {
        kumpulanMugiwara.put(id++, mugiwara);
        return "Sukses";
    }

    @PUT
    @Path("{id}")
    //Update data objek baru dengan id tertentu
    public Map<String, Mugiwara> updateMugiwara(@PathParam("id") Integer id, Mugiwara newMugiwara){
        Map<String, Mugiwara> result =  new HashMap<>();
        result.put("oldMugiwara", kumpulanMugiwara.get(id));
        kumpulanMugiwara.replace(id, newMugiwara);
        result.put("newMugiwara", kumpulanMugiwara.get(id));
        return result;
    }

    @DELETE
    @Path("{id}")
    //Delete data objek dengan id tertentu
    public Mugiwara deleteMugiwara(@PathParam("id") Integer id) {
        Mugiwara mugiwara = kumpulanMugiwara.get(id);
        kumpulanMugiwara.remove(id);
        return mugiwara;
    }

    @DELETE
    // Delete keseluruhan data dengan mengembalikan id menjadi 1
    public String deleteAllMugiwara() {
        kumpulanMugiwara.clear();
        return "Delete Berhasil";
    }
}