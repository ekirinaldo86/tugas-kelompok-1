package tech.candra.models;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class Inventory {
    
    @NotBlank(message = "Tidak boleh kosong!")
    private String nama;

    @NotBlank(message = "Tidak boleh kosong!")
    private String kode;

    @NotNull(message = "Tidak boleh kosong!")
    @Min(value = 0, message = "Tidak boleh kurang dari 0!")
    private Double harga;

    @NotNull(message = "Tidak boleh kosong!")
    @Min(value = 0, message = "Tidak boleh kurang dari 0!")
    private Integer jumlah;

    private static int idPlus = 1;

    private int id;

    public Inventory(String nama, String kode, Double harga, Integer jumlah) {
        this.nama = nama;
        this.kode = kode;
        this.harga = harga;
        this.jumlah = jumlah;
    }

    public Inventory() {
        this.id = idPlus;
        idPlus++;
    } 

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public Double getHarga() {
        return this.harga;
    }

    public void setHarga(Double harga) {
        this.harga = harga;
    }

    public Integer getJumlah() {
        return this.jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
   

}

