
package tech.candra.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;


public class Mugiwara {
     
    //Validasi Judul
    //Memberikan Pesan error
    @NotBlank (message = "Nama Harus Ada!")
    public String nama;

    @NotBlank (message = "weapon Harus Ada!")
    public String weapon;

    @NotBlank (message = "Armor Harus Ada!")
    public String armor;

    @Min(value = 0, message = "Health Terkecil 0")
    @Max(value = 100, message = "Health Terbesar 100")
    public Integer health;

    public Mugiwara() {}

    public Mugiwara(String nama, String weapon, String armor, Integer health) {
        this.nama = nama;
        this.weapon = weapon;
        this.armor = armor;
        this.health =health;
    }
}
