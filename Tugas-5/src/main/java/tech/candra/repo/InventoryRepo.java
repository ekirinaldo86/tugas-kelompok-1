package tech.candra.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import javax.enterprise.context.ApplicationScoped;

import tech.candra.models.Inventory;

@ApplicationScoped
public class InventoryRepo {
    
    List<Inventory> inventories = new ArrayList<>();

    public List<Inventory> getInventories() {
        return inventories;
    }

    public Inventory getInventory(int id) {
        return inventories.get(findIndexById(id));
    }

    public void addInventory(Inventory theInventory) {
        System.out.println("constructor check inventory empty : " + checkInventory());
        inventories.add(theInventory);
    }

    public void updateInventory(int id, Inventory inventory) {
        inventories.set(findIndexById(id), inventory);
    }

    public void deleteInventory(int id) {
        inventories.remove(findIndexById(id));
    }

    public void deleteInventories() {
        inventories.clear();
    }

    public boolean checkInventory() {
        return inventories.isEmpty();
    }

    private int findIndexById(int id) {
        return IntStream.range(0, getInventories().size())
        .filter(index -> getInventories().get(index).getId() == id)
        .findFirst().orElseThrow();
    }

}

